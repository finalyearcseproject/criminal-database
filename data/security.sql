-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2015 at 04:00 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `security`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_badge`
--

CREATE TABLE IF NOT EXISTS `tbl_badge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_crime_photo`
--

CREATE TABLE IF NOT EXISTS `tbl_crime_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criminal_id` int(11) NOT NULL,
  `crime_record_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_crime_video`
--

CREATE TABLE IF NOT EXISTS `tbl_crime_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criminal_id` int(11) NOT NULL,
  `crime_record_id` int(11) NOT NULL,
  `video` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_criminal_photos`
--

CREATE TABLE IF NOT EXISTS `tbl_criminal_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criminal_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_criminal_photos`
--

INSERT INTO `tbl_criminal_photos` (`id`, `criminal_id`, `image`) VALUES
(1, 1, 'cartoon1.jpg'),
(2, 1, 'cartoon3.jpg'),
(3, 1, 'cartoon8.jpg'),
(4, 2, 'cartoon10.jpg'),
(5, 3, '12img.jpg'),
(6, 3, '28cartoon4.jpg'),
(7, 4, '13cartoon10.jpg'),
(8, 4, '33cartoon7.jpg'),
(9, 4, '23cartoon1.jpg'),
(10, 4, '24cartoon6.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_criminal_records`
--

CREATE TABLE IF NOT EXISTS `tbl_criminal_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `criminal_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `location` varchar(255) NOT NULL,
  `crime_type` int(11) NOT NULL,
  `act_no` int(11) NOT NULL,
  `duty_officer` text NOT NULL,
  `description` text NOT NULL,
  `crime_photo` int(11) NOT NULL,
  `crime_video` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_crim_prsn`
--

CREATE TABLE IF NOT EXISTS `tbl_crim_prsn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `father_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `siblings` varchar(255) NOT NULL,
  `nationality` varchar(39) NOT NULL,
  `national_id` varchar(255) NOT NULL,
  `religion` varchar(12) NOT NULL,
  `blood_group` varchar(5) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `maretial_status` varchar(10) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `police_station` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `characteristics` text NOT NULL,
  `organizetion` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_crim_prsn`
--

INSERT INTO `tbl_crim_prsn` (`id`, `name`, `father_name`, `mother_name`, `siblings`, `nationality`, `national_id`, `religion`, `blood_group`, `sex`, `maretial_status`, `status`, `created_at`, `updated_at`, `division_id`, `district_id`, `police_station`, `rating`, `characteristics`, `organizetion`, `comments`) VALUES
(1, 'Tasnim Ahmed', 'Md. Abdul Hs', 'tasnim ka ma', 'tasnim ka vai', 'american', '123456789', 'Muslim', 'AB-', 'MALE', 'Divorsed', 5, '2015-12-01 09:55:43', '0000-00-00 00:00:00', 11, 3, 1, 3, 'very bad', 'ISI', 'rapist, Fucker. 100 beti Fucked by him '),
(2, 'Joy', 'Md. Abdul Hs', 'tasnim ka ma', 'tasnim ka vai', 'american', '123456789666', 'Muslim', 'AB-', 'MALE', 'Divorsed', 5, '2015-12-06 07:31:21', '0000-00-00 00:00:00', 11, 3, 1, 3, 'very bad', 'ISI', 'rapist, Fucker. 100 beti Fucked by him '),
(3, 'Md Rumel', 'test', 'test', 'tasnim ka vai', 'Bangladeshi', '12324234234242', 'Muslim', 'B+', 'MALE', 'Divorsed', 3, '2015-12-01 18:13:42', '0000-00-00 00:00:00', 11, 3, 1, 5, 'asdasdasd', 'asdasd', 'asdasdasdasdasdasd'),
(4, 'samit chanda', 'asdjhg', 'hgtjuyk', 'ftjtjvuytkb', 'Bangladeshi', '985432', 'Buddho', 'A+', 'MALE', 'Married', 3, '2015-12-06 07:39:37', '0000-00-00 00:00:00', 11, 3, 1, 5, 'tfhvtkuybvkn', 'ISI', 'rchtvjtk');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_crim_status`
--

CREATE TABLE IF NOT EXISTS `tbl_crim_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_crim_status`
--

INSERT INTO `tbl_crim_status` (`id`, `name`) VALUES
(1, 'Closed'),
(2, 'Died'),
(3, 'On Bail'),
(4, 'Case Running'),
(5, 'On Jail');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_district`
--

CREATE TABLE IF NOT EXISTS `tbl_district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `division_id` (`division_id`),
  KEY `division_id_2` (`division_id`),
  KEY `division_id_3` (`division_id`),
  KEY `division_id_4` (`division_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_district`
--

INSERT INTO `tbl_district` (`id`, `division_id`, `name`) VALUES
(3, 11, 'Sunamgonj'),
(4, 7, 'Bandorban');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_division`
--

CREATE TABLE IF NOT EXISTS `tbl_division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_division`
--

INSERT INTO `tbl_division` (`id`, `name`) VALUES
(6, 'Dhaka'),
(7, 'Chittagong'),
(8, 'Rajshahi'),
(9, 'Khulna'),
(10, 'Barishal'),
(11, 'Sylhet'),
(12, 'Rangpur');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE IF NOT EXISTS `tbl_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_ip` varchar(50) NOT NULL,
  `mac` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`id`, `name`, `username`, `user_type`, `password`, `user_ip`, `mac`) VALUES
(1, 'Joy', 'admin', 'admin', 'admin', '', ''),
(2, 'Joy', 'police', 'police', 'admin', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_police_address`
--

CREATE TABLE IF NOT EXISTS `tbl_police_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `police_id` int(11) NOT NULL,
  `presentAddress` text NOT NULL,
  `parmanentAddress` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `police_id` (`police_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_police_badges`
--

CREATE TABLE IF NOT EXISTS `tbl_police_badges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `police_id` int(11) NOT NULL,
  `badges` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `police_id` (`police_id`,`badges`),
  KEY `badges` (`badges`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_police_education`
--

CREATE TABLE IF NOT EXISTS `tbl_police_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `police_id` int(11) NOT NULL,
  `degree_name` varchar(255) NOT NULL,
  `passing_year` varchar(20) NOT NULL,
  `institute_name` varchar(50) NOT NULL,
  `board` varchar(50) NOT NULL,
  `file` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `police_id` (`police_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_police_postinfo`
--

CREATE TABLE IF NOT EXISTS `tbl_police_postinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `police_id` int(11) NOT NULL,
  `joining_date` date NOT NULL,
  `current_post` int(11) NOT NULL,
  `leaving_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `police_id` (`police_id`),
  KEY `current_post` (`current_post`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_police_primaryinfo`
--

CREATE TABLE IF NOT EXISTS `tbl_police_primaryinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `station_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `nationality` varchar(50) NOT NULL,
  `religion` varchar(50) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `maretial_status` varchar(30) NOT NULL,
  `national_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `division_id` (`division_id`),
  KEY `district_id` (`district_id`),
  KEY `station_id` (`station_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_police_records`
--

CREATE TABLE IF NOT EXISTS `tbl_police_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `police_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `police_id` (`police_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE IF NOT EXISTS `tbl_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_station`
--

CREATE TABLE IF NOT EXISTS `tbl_station` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `division_id` (`division_id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_station`
--

INSERT INTO `tbl_station` (`id`, `division_id`, `district_id`, `name`) VALUES
(1, 11, 3, 'Chhatak'),
(2, 7, 4, 'Nilgiri');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_district`
--
ALTER TABLE `tbl_district`
  ADD CONSTRAINT `tbl_district_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `tbl_division` (`id`);

--
-- Constraints for table `tbl_police_address`
--
ALTER TABLE `tbl_police_address`
  ADD CONSTRAINT `tbl_police_address_ibfk_1` FOREIGN KEY (`police_id`) REFERENCES `tbl_police_primaryinfo` (`id`);

--
-- Constraints for table `tbl_police_badges`
--
ALTER TABLE `tbl_police_badges`
  ADD CONSTRAINT `tbl_police_badges_ibfk_1` FOREIGN KEY (`police_id`) REFERENCES `tbl_police_postinfo` (`police_id`),
  ADD CONSTRAINT `tbl_police_badges_ibfk_2` FOREIGN KEY (`badges`) REFERENCES `tbl_badge` (`id`);

--
-- Constraints for table `tbl_police_education`
--
ALTER TABLE `tbl_police_education`
  ADD CONSTRAINT `tbl_police_education_ibfk_1` FOREIGN KEY (`police_id`) REFERENCES `tbl_police_address` (`police_id`);

--
-- Constraints for table `tbl_police_postinfo`
--
ALTER TABLE `tbl_police_postinfo`
  ADD CONSTRAINT `tbl_police_postinfo_ibfk_1` FOREIGN KEY (`police_id`) REFERENCES `tbl_police_records` (`police_id`),
  ADD CONSTRAINT `tbl_police_postinfo_ibfk_2` FOREIGN KEY (`current_post`) REFERENCES `tbl_post` (`id`);

--
-- Constraints for table `tbl_police_primaryinfo`
--
ALTER TABLE `tbl_police_primaryinfo`
  ADD CONSTRAINT `tbl_police_primaryinfo_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `tbl_station` (`division_id`),
  ADD CONSTRAINT `tbl_police_primaryinfo_ibfk_2` FOREIGN KEY (`district_id`) REFERENCES `tbl_station` (`district_id`),
  ADD CONSTRAINT `tbl_police_primaryinfo_ibfk_3` FOREIGN KEY (`station_id`) REFERENCES `tbl_station` (`id`);

--
-- Constraints for table `tbl_police_records`
--
ALTER TABLE `tbl_police_records`
  ADD CONSTRAINT `tbl_police_records_ibfk_1` FOREIGN KEY (`police_id`) REFERENCES `tbl_police_education` (`police_id`);

--
-- Constraints for table `tbl_station`
--
ALTER TABLE `tbl_station`
  ADD CONSTRAINT `tbl_station_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `tbl_district` (`division_id`),
  ADD CONSTRAINT `tbl_station_ibfk_2` FOREIGN KEY (`district_id`) REFERENCES `tbl_district` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
