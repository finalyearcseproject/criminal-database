-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2016 at 10:58 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `security`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gd`
--

CREATE TABLE IF NOT EXISTS `tbl_gd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to` varchar(255) NOT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `station_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `dacuments` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  `gd_no` varchar(255) NOT NULL,
  `gd_status` varchar(255) NOT NULL DEFAULT 'Pending',
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_gd`
--

INSERT INTO `tbl_gd` (`id`, `to`, `division_id`, `district_id`, `station_id`, `subject`, `description`, `dacuments`, `user_id`, `gd_no`, `gd_status`, `date`) VALUES
(1, 'sasda', 11, 3, 1, 'dsdfs', 'sdfsdf', 'sdfsdf', 1, '', '', '2016-02-04'),
(2, 'sasda', 11, 3, 1, 'dsdfs', 'sdfsdf', 'sdfsdf', 1, '123', 'Approved', '2016-02-06'),
(3, 'sasda', 11, 3, 1, 'dsdfs', 'sdfsdf', 'sdfsdf', 1, '123', 'Approved', '2016-02-08'),
(4, 'sasda', 11, 3, 1, 'dsdfs', 'sdfsdf', 'sdfsdf', 1, '123', '', '2016-02-10'),
(5, 'Officer Incharge', 11, 3, 1, 'lost certificate', 'i lost my certificate', 'roll:102498, reg:134256', 0, '', 'pending', '2016-02-03'),
(6, 'Officer Incharge', 0, 0, 0, 'lost certificate', 'i lost my certificate', 'roll:102498, reg:134256', 0, '', 'pending', '2016-02-03');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
