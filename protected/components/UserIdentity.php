<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
   
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user= Login::model()->findByAttributes(array('username'=>$this->username));
               
                $usermac = Login::model()->Findmac();
                
		if($user===null){
			$this->errorCode=self::ERROR_USERNAME_INVALID;
                }
		else if($user->password !== $this->password){
                   
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
                }
               /* else if($user->mac !== $usermac){
                        $this->errorCode=self::ERROR_MAC_INVALID;
                }*/
		else
		{
			$this->_id=$user->id;
			$this->username=$user->username;
                        Yii::app()->user->setState('username', $user->username);
			$this->errorCode=self::ERROR_NONE;
		}
		return $this->errorCode;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}