<?php

class SiteController extends Controller {

    public $layout = 'column1';

    /**
	 * Author :
	 * Joyprokash Chakraborty
         * joy.1993@hotmail.com
    */

    /**
     * Displays the login page
     */
    public function actionLogin() {

        /*if (!defined('CRYPT_BLOWFISH') || !CRYPT_BLOWFISH)
            throw new CHttpException(500, "This application requires that PHP was compiled with Blowfish support for crypt().");

        $model = new LoginForm;

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()){
               
                $this->redirect(array("dashboard/index"));
            }
        }*/
        // display the login form
        $this->renderPartial('commingsoon');
    }

     /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
