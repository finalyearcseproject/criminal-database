<?php

class CriminalController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function actionIndex() {
        $model = new Crimpersonalinfo;
        $CriminalStatusDrop = CHtml::listData(Criminalstatus::model()->findAll(), 'id', 'name');
        $DivisionList = CHtml::listData(Division::model()->findAll(), 'id', 'name');
        if (isset($_POST['Crimpersonalinfo'])) {
            $model->attributes = $_POST['Crimpersonalinfo'];
            if ($model->validate()) {
                $model->rating = $_POST['rating1'];
                if($model->save()){
                $type = isset($_GET['type']) ? $_GET['type'] : 'post';

                $photos = CUploadedFile::getInstancesByName('Crimpersonalinfo[file]');

                    if (isset($photos) && count($photos) > 0) {


                        foreach ($photos as $image => $pic) {
                            $rnNumber = rand(0, 40);
                            if ($pic->saveAs(Yii::getPathOfAlias('webroot') . '/images/' . $rnNumber.$pic->name)) {

                                $img_add = new Criminalphotos();
                                $img_add->image = $rnNumber.$pic->name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
                                $img_add->criminal_id = $model->id; // this links your picture model to the main model (like your user, or profile model)

                                $img_add->save(); // DONE
                            } else {
                                Yii::app()->user->setFlash('danger', 'Insert Error !!!');
                            }
                        }
                    }
                    Yii::app()->user->setFlash('success', 'Data Saved successfully');
               
                
            }
            
           }
        }
        $this->render('_form', array('model' => $model, 'CriminalStatusDrop' => $CriminalStatusDrop, 'DivisionList' => $DivisionList));
    }

    public function actionGetdistrict() {
        $id = $_GET['category_id'];
        $districtList = District::model()->findAllByAttributes(array('division_id' => $id));
        $dropList = "";
        $dropList.="<option value=''>Select District</option>";
        foreach ($districtList as $value) {
            $dropList.="<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
        }

        $data = array(
            'success' => 'success',
            'dropList' => $dropList,
        );

        echo json_encode($data);
    }

    public function actionGetstation() {
        $divisionid = $_GET['category_id'];
        $ds_id = $_GET['ds_id'];
        $districtList = Station::model()->findAllByAttributes(array('division_id' => $divisionid, 'district_id' => $ds_id));
        $dropList = "";
        $dropList.="<option value='' >Select Station</option>";
        foreach ($districtList as $value) {
            $dropList.="<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
        }

        $data = array(
            'success' => 'success',
            'dropList' => $dropList,
        );

        echo json_encode($data);
    }
    
    
    public function actionCriminalList(){
        $list = Crimpersonalinfo::model()->findAll();
        $this->render('page',array('list'=>$list));
    }
    
    public function actionSearch(){
        $division = Division::model()->findAll();
        $dropList = "";
        $dropList.="<option value=''>Select Division</option>";
        foreach ($division as $value) {
            $dropList.="<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
        }
        
        $status = Criminalstatus::model()->findAll();
        $dropLists = "";
        $dropLists.="<option value=''>Select Status</option>";
        foreach ($status as $value) {
            $dropLists.="<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
        }
        $this->render('criminalpage',array('dropList'=>$dropList,'dropLists'=>$dropLists));
    }
    
     public function actionGetone($id){
         
        $criminalInfo  = Crimpersonalinfo::model()->findByPk($id);
        $criminalPhoto = Criminalphotos::model()->findAllByAttributes(array('criminal_id'=>$id));
        
        $this->render('criminalpage',array('criminalInfo'=>$criminalInfo,'photos'=>$criminalPhoto));
    }
    
    public function actionFind(){
        $name = $_GET['name'];
        $nid = $_GET['nid'];
        $division = $_GET['division'];
        $district = $_GET['district'];
        $police = $_GET['police'];
        $rating = $_GET['rating'];
        $status = $_GET['status'];
        $getAll = Crimpersonalinfo::model()->getCriminals($name,$nid,$division,$district,$police,$rating,$status);
        //print_r($getAll);exit;
        $detail = '';
        foreach ($getAll as $value){
         $image = Criminalphotos::model()->findByAttributes(array('criminal_id' => $value['id']),array('order'=>'id')); 
         $status = Criminalstatus::model()->findByAttributes(array('id' => $value['status'])); 
         $rat = '';
         for($i = 0;$i < $value['rating'];$i++){
           $rat.='<i class="fa fa-star"></i>&nbsp;&nbsp;';
         }
          $detail.='<a href="#" onclick="showDetails(this)" data-value='.$value['id'].'><div  style="width:320px;height:180px;-webkit-border-radius: 25px;-moz-border-radius: 25px;border-radius: 25px;border:4px solid #23B919;background-color:black;float: left;margin: auto 3px;margin-top: 5px;" >
                        <div style="width:100px;float: left; height:100px; margin-bottom: 10px; margin-left: 10px; margin-top: 10px;">
                        <img src='.Yii::app()->request->baseUrl . '/images/'.$image->image.' style="width:100px;height:100px;" ></img>
                        </div>
                    <div style="width:180px; margin-top: 10px;float: left; height:100px; margin-left: 10px; margin-top: 10px;">
                       <p style="color:white"><b>Name        : '.$value['name'].'</b></p>
                       <p style="color:white"><b>Religion        : '.$value['religion'].'</b></p>
                       <p style="color:white"><b>'.$value['nationality'].'&nbsp;,&nbsp;'.$value['sex'].'</b></p>
                       <p style="color:white"><b>Status : &nbsp;&nbsp;'. $status->name.'</b></p>
                       <p style="color:white"><b>NID : &nbsp;&nbsp;'. $value['national_id'].'</b></p>
                   </div>
         
        <div style="color:white;margin-top: 13px;margin-right: 50pc;width: 80px;margin-left: 1pc;font-size: smaller;color:yellow">
        '.$rat.'</div>
        </div></a>';  
        }
        if($detail == ''){
           $data = array(
            'success' => 'success',
            'detail' => '<h3>No Records Found</h3>',
        );  
        }else{
        
        $data = array(
            'success' => 'success',
            'detail' => $detail,
        );
        }
        echo json_encode($data);
    }
    
    
    public function actionGetdetail(){
        $id = $_GET['id'];
        $getAll = Crimpersonalinfo::model()->findByAttributes(array('id'=>$id));
        
        $detail = '';
        
         $image = Criminalphotos::model()->findByAttributes(array('criminal_id' => $getAll->id),array('order'=>'id')); 
         $status = Criminalstatus::model()->findByAttributes(array('id' => $getAll->status)); 
         $rat = '';
         for($i = 0;$i < $getAll->rating;$i++){
           $rat.='<i class="fa fa-star" style="color:yellow"></i>&nbsp;&nbsp;';
         }
          
         $detail.='<div class="container">
      <a href='.Yii::app()->createUrl('admin/criminal/update',array('id'=>$getAll->id)).' ><i style="font-size: 15px;" class="fa fa-pencil-square-o" ></i></a>        
    <div class="row">
   
        <div class="col-md-3">
		 <img src='.Yii::app()->request->baseUrl . '/images/'.$image->image.' style="height: 200px;width: 266px;" ></img><br/>
                 
		</div>
        <div class="col-md-7" style="background-color: black;width: 555px;margin-left: 16px;margin-top: 10px;">
        <div> '.$rat.'</div><br/>
        <p style="color:white"><b>Name        : '.$getAll->name.'</b></p>
                       <p style="color:white"><b>Religion        : '.$getAll->religion.'</b></p>
                       <p style="color:white"><b>'.$getAll->nationality.'&nbsp;,&nbsp;'.$getAll->sex.'</b></p>
                       <p style="color:white"><b>Status : &nbsp;&nbsp;'. $getAll->name.'</b></p>
                       <p style="color:white"><b>NID : &nbsp;&nbsp;'. $getAll->national_id.'</b></p>

        </div>
    </div>
   
</div>'; 
        
        if($detail == ''){
           $data = array(
            'success' => 'success',
            'detail' => '<h3>No Records Found</h3>',
        );  
        }else{
        
        $data = array(
            'success' => 'success',
            'detail' => $detail,
        );
        }
        echo json_encode($data);
        
    }

    
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $CriminalStatusDrop = CHtml::listData(Criminalstatus::model()->findAll(), 'id', 'name');
        $DivisionList = CHtml::listData(Division::model()->findAll(), 'id', 'name');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Crimpersonalinfo'])) {
            $model->attributes = $_POST['Crimpersonalinfo'];
            if ($model->validate()) {
                $model->rating = $_POST['rating1'];
                if($model->save()){
                $type = isset($_GET['type']) ? $_GET['type'] : 'post';

                $photos = CUploadedFile::getInstancesByName('Crimpersonalinfo[file]');

                    if (isset($photos) && count($photos) > 0) {


                        foreach ($photos as $image => $pic) {
                            $rnNumber = rand(0, 40);
                            if ($pic->saveAs(Yii::getPathOfAlias('webroot') . '/images/' . $rnNumber.$pic->name)) {

                                $img_add = new Criminalphotos();
                                $img_add->image = $rnNumber.$pic->name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
                                $img_add->criminal_id = $model->id; // this links your picture model to the main model (like your user, or profile model)

                                $img_add->save(); // DONE
                            } else {
                                Yii::app()->user->setFlash('danger', 'Insert Error !!!');
                            }
                        }
                    }
                    Yii::app()->user->setFlash('success', 'Data Saved successfully');
               
                
            }
            
           }
        }

        $this->render('index', array(
            'model' => $model,'CriminalStatusDrop'=>$CriminalStatusDrop,'DivisionList'=>$DivisionList
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */

    
    public function loadModel($id) {
        $model = Crimpersonalinfo::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    

}
