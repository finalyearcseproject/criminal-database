<?php

class DashboardController extends Controller {

    public $layout = 'main';

    /**
	 * Author :
	 * Joyprokash Chakraborty
         * joy.1993@hotmail.com
    */

    /**
     * Displays the Dashboard page
     */
   
    public function actionIndex() {
        $this->render('index');
    }

}
