<?php
/* @var $this GdController */
/* @var $model Gd */
/* @var $form CActiveForm */
?>

<div class="row">
    <div class="col-md-2">
    </div>
    
    <div class="col-md-8">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gd-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'to'); ?>
		<?php echo $form->textField($model,'to',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'to'); ?>
	</div>

	
	<div class="form-group">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dacuments'); ?>
		<?php echo $form->textArea($model,'dacuments',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'dacuments'); ?>
	</div>
        <div class="form-group">
		<?php echo $form->labelEx($model,'gd_status'); ?>
		<?php echo $form->dropDownList($model,'gd_status',array('Approved'=>'Approved','Pending'=>'Pending','Denied'=>'Denied'),array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'gd_status'); ?>
	</div>
        <div class="form-group">
		<?php echo $form->labelEx($model,'gd_no'); ?>
		<?php echo $form->textField($model,'gd_no',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'gd_no'); ?>
	</div>
	

	<div class="form-group">
		
                <?php echo CHtml::submitButton('submit',array('class'=>'btn btn-success')); ?>
	</div>
</div>
    <div class="col-md-2">
        
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->