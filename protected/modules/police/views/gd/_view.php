<?php
/* @var $this GdController */
/* @var $data Gd */

?>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
 <h4><i class="fa fa-th-list"></i> General Diary List</h4><hr>
<div class="row">

	<table id="example1" class="table table-bordered">
            <thead>
                <tr><td>SN</td><td>To</td><td>Subject</td><td>Description</td> <td>Dacuments</td> <td>User ID</td><td>Gd Status</td><td>Gd No</td><td>Date</td><td>Action</td></tr>
            </thead>
            <tbody>
            <?php $i=1;foreach($data as $value){ ?>
                <tr><td><?php echo $i; ?></td> <td><?php echo $value['to']; ?></td> <td><?php echo $value['subject']; ?></td><td><?php echo $value['description']; ?></td><td><?php echo $value['dacuments']; ?></td><td><?php echo $value['user_id']; ?></td><td><?php echo $value['gd_status']; ?></td><td><?php echo $value['gd_no']; ?></td><td><?php echo $value['date']; ?></td><td> &nbsp  <a href="<?php echo Yii::app()->createUrl('police/gd/update',array('id'=>$value['id'])); ?>" ><i style="font-size: 15px;" class="fa fa-pencil-square-o" ></i></a></td></tr>
            <?php $i++;} ?>
            </tbody>
        </table>

</div>
 
 <script type="text/javascript">
                $(function() {
                    $("#example1").dataTable({ 
                        "iDisplayLength": 5,

            });
                    $('#example2').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": true
                    });
                });
        </script>