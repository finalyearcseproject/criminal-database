
<!DOCTYPE html>
 <html class="no-js" lang="en" > <!--<![endif]-->

  <head>  	
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
	<meta name="description" content="fresh Gray Bootstrap 3.0 Responsive Theme "/>
	<meta name="keywords" content="Template, Theme, web, html5, css3, Bootstrap,Bootstrap 3.0 Responsive Login" />
	<meta name="author" content="Adsays"/>
    <!--<link rel="shortcut icon" href="favicon.png"> -->
    
	<title></title>
    <!-- Bootstrap core CSS -->
	
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/admin_resource/css/bootstrap.min.css" />
    <!-- Demo CSS -->
    
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/login_resource/demo.css" />
    
    <!-- Custom styles for this template -->
    
    <link rel="stylesheet" type="text/css"  href="<?php echo Yii::app()->request->baseUrl; ?>/login_resource/login-theme-1.css" />
    
	</head>
    <body >
    	
    	<div class="container" id="login-block">
    		<div class="row">
			    <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
			    	 
			       <div class="login-box clearfix animated flipInY">
			       		<div class="page-icon animated bounceInDown">
			       			
			       		</div>
			        	<div class="login-logo">
			        		
			        	</div> 
			        	<hr />
			        	<div class="login-form">
			        		<!-- Start Error box -->
			        		<div class="alert alert-danger hide">
								  <button type="button" class="close" data-dismiss="alert"> &times;</button>
								  <h4>Error!</h4>
								 
							</div> <!-- End Error box -->
							<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'login-form',
							'enableClientValidation'=>true,
							'clientOptions'=>array(
							'validateOnSubmit'=>true,
							),
							)); ?>
							<?php echo $form->textField($model,'user_id',array('size'=>50,'maxlength'=>100,'class'=>'input-field','placeholder'=>'Username')); ?>
							<?php echo $form->error($model,'user_id'); ?>
							<?php echo $form->passwordField($model,'password',array('size'=>50,'maxlength'=>100,'class'=>'input-field','placeholder'=>'Password')); ?>
							<?php echo $form->error($model,'password'); ?>
							<?php echo CHtml::submitButton('Login',array('class'=>'btn btn-login')); ?>
							<?php $this->endWidget(); ?>
			        		
							<div class="login-links"> 
					            <a href="forgot-password.html">
					          	 
					            </a>
					            <br />
					            <a href="sign-up.html">
					            </a>
							</div>      		
			        	</div> 			        	
			       </div>
			        
			  
			        
			    </div>
			</div>
    	</div>
     
      	<!-- End Login box -->
     	<footer class="container">
     		<p id="footer-text"><small>&copy; Copyright 2014 <a target="_blank" href="http://www.facebook.com/joycse.cb">Developed By Joy</a></small></p>
     	</footer>
		<link rel="script" type="text/javascript" href="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"/>
        
       
       
		<link rel="script" type="text/javascript" href="<?php echo Yii::app()->request->baseUrl; ?>/login_resource/bootstrap.min.js"/>	
		<link rel="script" type="text/javascript" href="<?php echo Yii::app()->request->baseUrl; ?>/login_resource/placeholder-shim.min.js"/>			
         <link rel="script" type="text/javascript" href="<?php echo Yii::app()->request->baseUrl; ?>/login_resource/custom.js"/>     
    </body>
</html>
