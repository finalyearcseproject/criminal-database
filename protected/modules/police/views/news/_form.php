<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<?php if (Yii::app()->user->hasFlash('success')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-success"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>
<?php if (Yii::app()->user->hasFlash('danger')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-danger"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>



<div class="row">
    
    <div class="col-md-6">
         <h4> <i class="fa fa-plus-circle"></i> Add News </h4> <hr>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'division-form',
	'enableAjaxValidation'=>FALSE,
    'enableClientValidation'=>true,
    'clientOptions'=>array('validateOnSubmit'=>true, 'validateOnChange'=>true),
)); ?>

	

	<?php //echo $form->errorSummary($model); ?>
         <div class="form-group">
        <?php echo $form->labelEx($model,'Select Priority'); ?>
        
       <?php echo $form->dropDownList($model,'priority',$drop,array('class'=>'form-control','style'=>'width: 122px')); ?>
        </div>

	<div class="form-group">
            
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date',array('class'=>'form-control','id'=>'dp1','style'=>'width: 122px','dateFormat:yy-mm-dd')); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>
         
         <div class="form-group">
		<?php echo $form->labelEx($model,'News Headline'); ?>
		<?php echo $form->textArea($model,'news_title',array('rows'=>2, 'cols'=>80)); ?>
		<?php //echo $form->error($model,'news_title'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'News Description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>80)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	

	<div class="form-group">
		
                <?php echo CHtml::submitButton('submit',array('class'=>'btn btn-success')); ?>
	</div>
        
        </div>
    
    <div class="col-md-6">
        
         <h4> <i class="fa fa-bullhorn"></i> News </h4> <hr>
         
         <?php $data= News::model()->findAll();   ?>
         
         <table id="example1" class="table table-bordered">
            <thead>
            <tr><td>SN</td><td>Date</td><td>News Headline</td><td>News</td><td>Priority</td><td>Action</td></tr>
            </thead>
            <tbody>
            <?php $i=1;foreach($data as $value){ ?>
                <tr><td><?php echo $i; ?></td> <td><?php echo $value['date']; ?></td> <td><?php echo $value['news_title']; ?></td><td><?php echo $value['description']; ?></td><td><?php echo News::model()->getPriority($value['priority']); ?></td><td><a href="<?php echo Yii::app()->createUrl('admin/news/delete',array('id'=>$value['id'])); ?>"  onclick="return confirm('Are you sure want to delete');"><i style="font-size: 15px;" class="fa fa-trash-o"></i></a> &nbsp  <a href="<?php echo Yii::app()->createUrl('admin/news/update',array('id'=>$value['id'])); ?>" ><i style="font-size: 15px;" class="fa fa-pencil-square-o" ></i></a></td></tr>
            <?php $i++;} ?>
            </tbody>
        </table>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->