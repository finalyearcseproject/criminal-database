<?php

/**
 * This is the model class for table "{{criminal_records}}".
 *
 * The followings are the available columns in table '{{criminal_records}}':
 * @property integer $id
 * @property integer $criminal_id
 * @property string $date
 * @property string $location
 * @property integer $crime_type
 * @property integer $act_no
 * @property string $duty_officer
 * @property string $description
 * @property integer $crime_photo
 * @property integer $crime_video
 */
class Crimerecords extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{criminal_records}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('criminal_id, date, location, crime_type, act_no, duty_officer, description, crime_photo, crime_video', 'required'),
			array('criminal_id, crime_type, act_no, crime_photo, crime_video', 'numerical', 'integerOnly'=>true),
			array('location', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, criminal_id, date, location, crime_type, act_no, duty_officer, description, crime_photo, crime_video', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'criminal_id' => 'Criminal',
			'date' => 'Date',
			'location' => 'Location',
			'crime_type' => 'Crime Type',
			'act_no' => 'Act No',
			'duty_officer' => 'Duty Officer',
			'description' => 'Description',
			'crime_photo' => 'Crime Photo',
			'crime_video' => 'Crime Video',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('criminal_id',$this->criminal_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('crime_type',$this->crime_type);
		$criteria->compare('act_no',$this->act_no);
		$criteria->compare('duty_officer',$this->duty_officer,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('crime_photo',$this->crime_photo);
		$criteria->compare('crime_video',$this->crime_video);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Crimerecords the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
