<?php

/**
 * This is the model class for table "{{gd}}".
 *
 * The followings are the available columns in table '{{gd}}':
 * @property integer $id
 * @property string $to
 * @property integer $division_id
 * @property integer $district_id
 * @property integer $station_id
 * @property string $subject
 * @property string $description
 * @property string $dacuments
 * @property integer $user_id
 */
class Gd extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{gd}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('to, division_id, district_id, station_id, subject, description, dacuments, user_id', 'required'),
			array('division_id, district_id, station_id, user_id', 'numerical', 'integerOnly'=>true),
			array('to, subject', 'length', 'max'=>255),
                    array('gd_no', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, to, division_id, district_id, gd_status, gd_no,date, station_id, subject, description, dacuments, user_id', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'to' => 'To',
			'division_id' => 'Division',
			'district_id' => 'District',
			'station_id' => 'Station',
			'subject' => 'Subject',
			'description' => 'Description',
			'dacuments' => 'Dacuments',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('to',$this->to,true);
		$criteria->compare('division_id',$this->division_id);
		$criteria->compare('district_id',$this->district_id);
		$criteria->compare('station_id',$this->station_id);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('dacuments',$this->dacuments,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Gd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
