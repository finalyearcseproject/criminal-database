<?php

class PoliceprimaryinfoController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    /**
     * @return array action filters
     */
    /* public function filters()
      {
      return array(
      'accessControl', // perform access control for CRUD operations
      'postOnly + delete', // we only allow deletion via POST request
      );
      } */

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    /* public function accessRules()
      {
      return array(
      array('allow',  // allow all users to perform 'index' and 'view' actions
      'actions'=>array('index','view'),
      'users'=>array('*'),
      ),
      array('allow', // allow authenticated user to perform 'create' and 'update' actions
      'actions'=>array('create','update'),
      'users'=>array('@'),
      ),
      array('allow', // allow admin user to perform 'admin' and 'delete' actions
      'actions'=>array('admin','delete'),
      'users'=>array('admin'),
      ),
      array('deny',  // deny all users
      'users'=>array('*'),
      ),
      );
      } */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionIndex() {
        $model = new Policeprimaryinfo;
        $DivisionList = CHtml::listData(Division::model()->findAll(), 'id', 'name');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Policeprimaryinfo'])) {
            $model->attributes = $_POST['Policeprimaryinfo'];
            $uploadedFile = CUploadedFile::getInstance($model, 'image');
            $rnd = rand(0, 9999);
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name

            $model->image = $fileName;
            $model->uid = $rnd . "-" . $_POST['Policeprimaryinfo']['division_id'] . "-" . $_POST['Policeprimaryinfo']['district_id'] . "-" . $_POST['Policeprimaryinfo']['station_id'];
            //echo $model->uid;exit;
            if ($model->save())
                $uploadedFile->saveAs(Yii::getPathOfAlias('webroot') . '/images/' . $fileName);
            Yii::app()->user->setFlash('success', 'Data Saved Succesfully');
        }

        $this->render('_form', array(
            'model' => $model, 'DivisionList' => $DivisionList
        ));
    }

    public function actionsearch() {
        $division = Division::model()->findAll();
        $dropList = "";
        $dropList.="<option value=''>Select Division</option>";
        foreach ($division as $value) {
            $dropList.="<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
        }

        $status = Criminalstatus::model()->findAll();
        $dropLists = "";
        $dropLists.="<option value=''>Select Status</option>";
        foreach ($status as $value) {
            $dropLists.="<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
        }
        $this->render('policepage', array('dropList' => $dropList, 'dropLists' => $dropLists));
    }

    public function actionFind() {

        $division = $_GET['division'];
        $district = $_GET['district'];
        $police = $_GET['police'];

        $getAll = Policeprimaryinfo::model()->getPolices($division, $district, $police);
        //print_r($getAll);exit;
        $detail = '';
        foreach ($getAll as $value) {


            $detail.='<a href="#" onclick="showDetails(this)" data-value=' . $value['id'] . '><div  style="width:320px;height:180px;-webkit-border-radius: 25px;-moz-border-radius: 25px;border-radius: 25px;border:4px solid #23B919;background-color:black;float: left;margin: auto 3px;margin-top: 5px;" >
                                <div style="width:100px;float: left; height:100px; margin-bottom: 10px; margin-left: 10px; margin-top: 10px;">
                                <img src=' . Yii::app()->request->baseUrl . '/images/' . $value['image'] . ' style="width:100px;height:100px;" ></img>
                                </div>
                            <div style="width:180px; margin-top: 10px;float: left; height:100px; margin-left: 10px; margin-top: 10px;">
                               <p style="color:white"><b>Name        : ' . $value['name'] . '</b></p>
                               <p style="color:white"><b>Religion        : ' . $value['religion'] . '</b></p>
                               <p style="color:white"><b>' . $value['nationality'] . '&nbsp;,&nbsp;' . $value['sex'] . '</b></p>
                               
                               <p style="color:white"><b>NID : &nbsp;&nbsp;' . $value['national_id'] . '</b></p>
                               <p style="color:white"><b>Unique ID : &nbsp;&nbsp;' . $value['uid'] . '</b></p>
                           </div>

                </div></a>';
        }
        if ($detail == '') {
            $data = array(
                'success' => 'success',
                'detail' => '<h3>No Records Found</h3>',
            );
        } else {

            $data = array(
                'success' => 'success',
                'detail' => $detail,
            );
        }
        echo json_encode($data);
    }

    public function actionGetdetail() {
        $id = $_GET['id'];
        $getAll = Policeprimaryinfo::model()->findByAttributes(array('id' => $id));

        $detail = '';


        $detail.='<div class="container">
    <div class="row">
        <div class="col-md-3">
		 <img src=' . Yii::app()->request->baseUrl . '/images/' . $getAll->image . ' style="height: 200px;width: 266px;" ></img><br/>
                 
		</div>
      <div class="col-md-9" style="background-color:black;width: 550px;">
        <p style="color:white"><b>Name        : ' . $getAll->name . '</b></p>
                       <p style="color:white"><b>Religion        : ' . $getAll->religion . '</b></p>
                       <p style="color:white"><b>' . $getAll->nationality . '&nbsp;,&nbsp;' . $getAll->sex . '</b></p>
                       <p style="color:white"><b>Status : &nbsp;&nbsp;' . $getAll->status . '</b></p>
                       <p style="color:white"><b>NID : &nbsp;&nbsp;' . $getAll->national_id . '</b></p>
                      <p style="color:white"><b>Unique ID : &nbsp;&nbsp;' . $getAll->uid . '</b></p>
                      <p style="color:white"><b>Posting : &nbsp;&nbsp;' . Policeprimaryinfo::model()->findStation($getAll->division_id, $getAll->district_id, $getAll->station_id) . ' Thana</b></p>

        </div>
    </div>
   
</div>';

        if ($detail == '') {
            $data = array(
                'success' => 'success',
                'detail' => '<h3>No Records Found</h3>',
            );
        } else {

            $data = array(
                'success' => 'success',
                'detail' => $detail,
            );
        }
        echo json_encode($data);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Policeprimaryinfo'])) {
            $model->attributes = $_POST['Policeprimaryinfo'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Policeprimaryinfo('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Policeprimaryinfo']))
            $model->attributes = $_GET['Policeprimaryinfo'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Policeprimaryinfo the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Policeprimaryinfo::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Policeprimaryinfo $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'policeprimaryinfo-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
