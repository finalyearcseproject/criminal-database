<?php

class DistrictController extends Controller
{
	
	public $layout='main';

	
        public function actionIndex()
        {
            $model = new District;
            $drop = CHtml::listData(Division::model()->findAll(), 'id', 'name'); //short if else
            if(isset($_POST['District'])){
                $model->attributes = $_POST['District'];
                if($model->validate() && $model->save()){
                Yii::app()->user->setFlash('success','Data Saved Succesfully');    
                    
                }
            }
            $this->render('_form',array('model'=>$model,'drop'=>$drop));
            
        }

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['District']))
		{
			$model->attributes=$_POST['District'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
                Yii::app()->user->setFlash('danger','Data deleted Succesfully');
		$this->redirect(array('index'));
		
	
	}

	
	public function loadModel($id)
	{
		$model=District::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='district-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
       

   
}
