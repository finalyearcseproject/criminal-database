<?php

class StationController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='main';

	
        public function actionIndex() {
            
            $model = new Station;
            
            $drop = CHtml::listData(Division::model()->findAll(), 'id', 'name');
            $drop2 = CHtml::listData(District::model()->findAll(), 'id', 'name');
            
            if(isset($_POST['Station'])){
                $model->attributes = $_POST['Station'];
                if($model->validate()){
                 $model->save();
                Yii::app()->user->setFlash('success','Data Saved Succesfully');    
                    
                }
            }
            
            $this->render('_form',array('model'=>$model,'drop'=>$drop,'drop2'=>$drop2));
        }
        
        
        public function actionGetdistrict() {
        $id = $_GET['category_id'];
        $districtList = District::model()->findAllByAttributes(array('division_id' => $id));
        $dropList = "";
        $dropList.="<option>Select District</option>";
        foreach ($districtList as $value) {
            $dropList.="<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
        }

        $data = array(
            'success' => 'success',
            'dropList' => $dropList,
        );

        echo json_encode($data);
    }
        
        
        public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
        
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Station']))
		{
			$model->attributes=$_POST['Station'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		Yii::app()->user->setFlash('danger','Data deleted Succesfully');
		$this->redirect(array('index'));
		
	}

	public function loadModel($id)
	{
		$model=Station::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Station $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='station-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
