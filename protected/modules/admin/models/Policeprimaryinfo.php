<?php

/**
 * This is the model class for table "{{police_primaryinfo}}".
 *
 * The followings are the available columns in table '{{police_primaryinfo}}':
 * @property integer $id
 * @property integer $division_id
 * @property integer $district_id
 * @property integer $station_id
 * @property string $name
 * @property string $father_name
 * @property string $mother_name
 * @property string $sex
 * @property string $nationality
 * @property string $religion
 * @property string $blood_group
 * @property string $maretial_status
 * @property integer $national_id
 * @property string $image
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property PoliceAddress[] $policeAddresses
 * @property Station $division
 * @property Station $district
 * @property Station $station
 */
class Policeprimaryinfo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{police_primaryinfo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('division_id, district_id, station_id, name, father_name, mother_name, sex, nationality, religion, blood_group, maretial_status, national_id, image, status', 'required'),
			array('division_id, district_id, station_id, national_id', 'numerical', 'integerOnly'=>true),
			array('name, father_name, mother_name, image', 'length', 'max'=>255),
			array('sex, blood_group', 'length', 'max'=>10),
			array('nationality, religion', 'length', 'max'=>50),
			array('maretial_status', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, division_id, district_id, station_id, name, father_name, mother_name, sex, nationality, religion, blood_group, maretial_status, national_id, image, status,uid', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'policeAddresses' => array(self::HAS_MANY, 'PoliceAddress', 'police_id'),
			'division' => array(self::BELONGS_TO, 'Station', 'division_id'),
			'district' => array(self::BELONGS_TO, 'Station', 'district_id'),
			'station' => array(self::BELONGS_TO, 'Station', 'station_id'),
		);
	}
        
        public function getPolices($division,$district,$police){
            
            $connection = Yii::app()->db;
            if($district==null && $police==null){
               
                    $row = "select * from tbl_police_primaryinfo WHERE division_id='$division%'"; 
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo3 = $InFormation;
                    return $InFormation;
                    
               
                
            }else if($police==null){
                
                   $row = "select * from tbl_police_primaryinfo WHERE division_id='$division%' AND district_id='$district%'";
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo4 = $InFormation;
                    return $InFormation;
                    
              
                
            }
            else{
             
                  $row = "select * from tbl_police_primaryinfo WHERE  division_id = '$division%' AND district_id = '$district%' AND station_id ='$police%'";
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo7 = $InFormation;
                    return $InFormation;
                    
               
            }
            
          
        }
        
        public function findStation($division_id, $district_id){
            $connection = Yii::app()->db;
            $row = "select * from tbl_station WHERE  division_id = '$division_id' AND district_id = '$district_id'";
            $command = $connection->createCommand($row);
            $InFormation = $command->queryAll();
            //print_r($InFormation);exit;
            return $InFormation[0]['name'];
        }


        
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'division_id' => 'Division',
			'district_id' => 'District',
			'station_id' => 'Station',
			'name' => 'Name',
			'father_name' => 'Father Name',
			'mother_name' => 'Mother Name',
			'sex' => 'Sex',
			'nationality' => 'Nationality',
			'religion' => 'Religion',
			'blood_group' => 'Blood Group',
			'maretial_status' => 'Maretial Status',
			'national_id' => 'National ID',
			'image' => 'Image',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('division_id',$this->division_id);
		$criteria->compare('district_id',$this->district_id);
		$criteria->compare('station_id',$this->station_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('father_name',$this->father_name,true);
		$criteria->compare('mother_name',$this->mother_name,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('nationality',$this->nationality,true);
		$criteria->compare('religion',$this->religion,true);
		$criteria->compare('blood_group',$this->blood_group,true);
		$criteria->compare('maretial_status',$this->maretial_status,true);
		$criteria->compare('national_id',$this->national_id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Policeprimaryinfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
