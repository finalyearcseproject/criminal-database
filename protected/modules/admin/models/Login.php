<?php

/**
 * Author :
 * Joyprokash Chakraborty
 * joy.1993@hotmail.com
 */
class Login extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{login}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, username, password, mac', 'required'),
            array('name, username, password, mac', 'length', 'max' => 50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, username, password, mac', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    
    public function Findmac() {
        // * Getting MAC Address using PHP
        ob_start(); // Turn on output buffering

        system('ipconfig /all'); //Execute external program to display output

        $mycom = ob_get_contents(); // Capture the output into a variable

        ob_clean(); // Clean (erase) the output buffer

        $findme = "Physical";

        $pmac = strpos($mycom, $findme); // Find the position of Physical text

        $mac = substr($mycom, ($pmac + 36), 17); // Get Physical Address
        $finalMac = trim($mac);
        
      // echo $finalMac;exit;
        return $finalMac;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'password' => 'Password',
            'mac' => 'Mac',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('mac', $this->mac, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Login the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
