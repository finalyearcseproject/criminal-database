<?php

/**
 * This is the model class for table "{{crim_prsn}}".
 *
 * The followings are the available columns in table '{{crim_prsn}}':
 * @property integer $id
 * @property string $name
 * @property string $father_name
 * @property string $mother_name
 * @property string $siblings
 * @property string $nationality
 * @property string $religion
 * @property string $blood_group
 * @property string $sex
 * @property string $maretial_status
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $division_id
 * @property integer $district_id
 * @property integer $police_station
 * @property integer $rating
 * @property string $characteristics
 * @property string $organizetion
 * @property integer $comments
 */
class Crimpersonalinfo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{crim_prsn}}';
	}
        
        public $file;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, father_name, mother_name, nationality, religion, sex, status, division_id, district_id, police_station', 'required'),
			array('status, division_id, district_id, police_station, rating', 'numerical', 'integerOnly'=>true),
			array('name, father_name, mother_name', 'length', 'max'=>150),
			array('siblings, organizetion', 'length', 'max'=>255),
			array('nationality', 'length', 'max'=>39),
			array('religion', 'length', 'max'=>12),
			array('blood_group', 'length', 'max'=>5),
			array('sex, maretial_status', 'length', 'max'=>10),
			array('updated_at', 'safe'),
                        array('file','file','types'=>'jpg, png, gif', 'allowEmpty'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, father_name, mother_name, siblings, nationality, religion, blood_group, sex, maretial_status, status, created_at, updated_at, division_id, district_id, police_station, rating, characteristics, organizetion, comments,national_id,file', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'father_name' => 'Father Name',
			'mother_name' => 'Mother Name',
			'siblings' => 'Siblings',
			'nationality' => 'Nationality',
			'religion' => 'Religion',
			'blood_group' => 'Blood Group',
			'sex' => 'Sex',
			'maretial_status' => 'Maretial Status',
			'status' => 'Status',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'division_id' => 'Division',
			'district_id' => 'District',
			'police_station' => 'Police Station',
			'rating' => 'Rating',
			'characteristics' => 'Characteristics',
			'organizetion' => 'Organizetion',
			'comments' => 'Comments',
		);
	}
        
        public function getCriminals($name,$nid,$division,$district,$police,$rating,$status){
            
            $connection = Yii::app()->db;
            if($nid==null && $division==null && $district==null && $police==null &&  $rating== null && $status == null){
                if($name == @$tmpname){
                    return $tmpinfo1;
                }else{
                    $row = "select * from tbl_crim_prsn WHERE name LIKE '$name%'"; 
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo1 = $InFormation;
                    return $InFormation;
                }
            }else if($division==null && $district==null && $police==null &&  $rating== null && $status == null){
                if($name == @$tmpname && $nid== @$tmpnid){
                    return $tmpinfo2;
                }else{
                    $row = "select * from tbl_crim_prsn WHERE name LIKE '$name%' AND national_id LIKE '$nid%'";
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo2 = $InFormation;
                    return $InFormation;
                }
                 
            }else if($district==null && $police==null &&  $rating== null && $status == null){
                if($name == @$tmpname && $nid== @$tmpnid && $division == @$tmpdivision){
                    return $tmpinfo3;
                }else{
                    $row = "select * from tbl_crim_prsn WHERE name LIKE '$name%' AND national_id LIKE '$nid%' AND division_id LIKE '$division%'"; 
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo3 = $InFormation;
                    return $InFormation;
                    
                }
                
            }else if($police==null &&  $rating== null && $status == null){
                 if($name == @$tmpname && $nid== @$tmpnid && $division == @$tmpdivision && $district == @$tmpdistrict){
                    return $tmpinfo4;
                }else{
                   $row = "select * from tbl_crim_prsn WHERE name LIKE '$name%' AND national_id LIKE '$nid%' AND division_id LIKE '$division%' AND district_id LIKE '$district%'";
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo4 = $InFormation;
                    return $InFormation;
                    
                }
                
            }else if($rating== null && $status == null){
                 if($name == @$tmpname && $nid== @$tmpnid && $division == @$tmpdivision && $district == @$tmpdistrict && $police==@$tmppolice){
                    return $tmpinfo5;
                }else{
                   $row = "select * from tbl_crim_prsn WHERE name LIKE '$name%' AND national_id LIKE '$nid%' AND division_id LIKE '$division%' AND district_id LIKE '$district%' AND police_station LIKE '$police%'";
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo5 = $InFormation;
                    return $InFormation;
                    
                }
               
            }else if($status == null){
                 if($name == @$tmpname && $nid== @$tmpnid && $division == @$tmpdivision && $district == @$tmpdistrict && $police==@$tmppolice && $rating == @$tmprating){
                    return $tmpinfo6;
                }else{
                   $row = "select * from tbl_crim_prsn WHERE name LIKE '$name%' AND national_id LIKE '$nid%' AND division_id LIKE '$division%' AND district_id LIKE '$district%' AND police_station LIKE '$police%' AND rating LIKE '$rating%'";
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo6 = $InFormation;
                    return $InFormation;
                    
                }
                
            }else{
              if($name == @$tmpname && $nid== @$tmpnid && $division == @$tmpdivision && $district == @$tmpdistrict && $police==@$tmppolice && $rating == @$tmprating && $status == @$tmpstatus){
                    return $tmpinfo7;
                }else{
                  $row = "select * from tbl_crim_prsn WHERE name LIKE '$name%' AND national_id LIKE '$nid%' AND division_id LIKE '$division%' AND district_id LIKE '$district%' AND police_station LIKE '$police%' AND rating LIKE '$rating%' AND status LIKE '$status%' ";
                    $command = $connection->createCommand($row);
                    $InFormation = $command->queryAll();
                    $tmpinfo7 = $InFormation;
                    return $InFormation;
                    
                }  
            
            }
            
            
            $tmpname = $name;$tmpnid = $nid;$tmpdivision = $division;$tmpdistrict = $district;$tmppolice= $police;$tmprating = $rating;$tmpstatus = $status;
        
            
        }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('father_name',$this->father_name,true);
		$criteria->compare('mother_name',$this->mother_name,true);
		$criteria->compare('siblings',$this->siblings,true);
		$criteria->compare('nationality',$this->nationality,true);
		$criteria->compare('religion',$this->religion,true);
		$criteria->compare('blood_group',$this->blood_group,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('maretial_status',$this->maretial_status,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('division_id',$this->division_id);
		$criteria->compare('district_id',$this->district_id);
		$criteria->compare('police_station',$this->police_station);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('characteristics',$this->characteristics,true);
		$criteria->compare('organizetion',$this->organizetion,true);
		$criteria->compare('comments',$this->comments);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Crimpersonalinfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
