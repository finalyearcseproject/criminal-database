<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

<table><tr>
        
        <td>
            <div class="form-group">
                <label for="usr">Division:</label>

                <select class="form-control" id="division">
                    <?php echo $dropList; ?>
                </select>
            </div>
        </td>
        <td style="width: 10px;"></td>
        <td>
            <div class="form-group">
                <label for="usr">District:</label>

                <select class="form-control" id="district">
                    <option value="">Select District</option>
                </select>
            </div>
        </td>
        <td style="width: 10px;"></td>
        <td>
            <div class="form-group">
                <label for="usr">Police:</label>

                <select class="form-control" id="police">
                    <option value="">Select Station</option>
                </select>
            </div>
        </td>
         </tr>
</table><hr>
 <i class="fa fa-refresh fa-spin" id="loader" style="display:none;font-size: 82px;color: cornflowerblue;"></i>
<div class="row" id="des" >
 
</div>

    </div>
  <!--================================== Modal ====================================-->  
  <style>
      .modal.fade .modal-dialog {
    -webkit-transform: scale(0.1);
    -moz-transform: scale(0.1);
    -ms-transform: scale(0.1);
    transform: scale(0.1);
    top: 300px;
    opacity: 0;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    transition: all 0.3s;
}

.modal.fade.in .modal-dialog {
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
    -webkit-transform: translate3d(0, -300px, 0);
    transform: translate3d(0, -300px, 0);
    opacity: 1;
}
      
      
  </style>
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Police Information</h4>
      </div>
        <i class="fa fa-refresh fa-spin" id="loaders" style="display:none;font-size: 82px;color: cornflowerblue;"></i>
      <div class="modal-body" id = "sdfds">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
    function showDetails(val) {
    var value = val.getAttribute("data-value");
     $.ajax({
            url: "Getdetail",
            data: {"id": value},
            beforeSend: function () {
                $("#loaders").show();
                 $("#myModal").modal('show');
                
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                if(obj.success == "success"){
                 $("#loaders").hide();
               
                $("#sdfds").html(obj.detail);
               
               
                
            }
        }

        })
}

    
    
    $('#name').on('input', function () {
        
        var division = $("#division").val();
        var district = $("#district").val();
        var police = $("#police").val();
      
        $.ajax({
            url: "find",
            data: {"division": division, "district": district, "police": police},
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                if(obj.success == "success"){
                $("#des").empty();
                $("#des").append(obj.detail);
                $('#des').fadeIn('fast');
                $("#loader").hide();
                
            }
        }

        })
    });

    $('#id').on('input', function () {
        var name = $("#name").val();
        var nid = $("#id").val();
        var division = $("#division").val();
        var district = $("#district").val();
        var police = $("#police").val();
        var rating = $("#rating").val();
        var status = $("#status").val();
        $.ajax({
            url: "find",
            data: {"name": name, "nid": nid, "division": division, "district": district, "police": police, "rating": rating, "status": status},
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
                  var obj = jQuery.parseJSON(data);
                if(obj.success == "success"){
                $("#des").empty();
                $("#des").append(obj.detail);
                $('#des').fadeIn('fast');
                $("#loader").hide();
                
            }
            },


        })
    });

    $('#division').on('change', function () {
       
        var division = $("#division").val();
        var district = $("#district").val();
        var police = $("#police").val();
       
        $.ajax({
            url: "find",
            data: {"division": division, "district": district, "police": police},
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                if(obj.success == "success"){
                $("#des").empty();
                $("#des").append(obj.detail);
                $('#des').fadeIn('fast');
                $("#loader").hide();
                
            }
            },


        })
    });


    $('#district').on('change', function () {
       
        var division = $("#division").val();
        var district = $("#district").val();
        var police = $("#police").val();
       
        $.ajax({
            url: "find",
            data: {"division": division, "district": district, "police": police},
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                if(obj.success == "success"){
                $("#des").empty();
                $("#des").append(obj.detail);
                $('#des').fadeIn('fast');
                $("#loader").hide();
                
            }
            },


        })
    });

    $('#police').on('change', function () {
      
        var division = $("#division").val();
        var district = $("#district").val();
        var police = $("#police").val();
        
        $.ajax({
            url: "find",
            data: {"division": division, "district": district, "police": police, "rating": rating, "status": status},
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
               var obj = jQuery.parseJSON(data);
                if(obj.success == "success"){
                $("#des").empty();
                $("#des").append(obj.detail);
                $('#des').fadeIn('fast');
                $("#loader").hide();
                
            }
            },


        })
    });

    $('#status').on('change', function () {
        var name = $("#name").val();
        var nid = $("#id").val();
        var division = $("#division").val();
        var district = $("#district").val();
        var police = $("#police").val();
        var rating = $("#rating").val();
        var status = $("#status").val();
        $.ajax({
            url: "find",
            data: {"name": name, "nid": nid, "division": division, "district": district, "police": police, "rating": rating, "status": status},
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                if(obj.success == "success"){
                $("#des").empty();
                $("#des").append(obj.detail);
                $('#des').fadeIn('fast');
                $("#loader").hide();
                
            }
            }


        })
    });
    
    function aja(){
        var name = $("#name").val();
        var nid = $("#id").val();
        var division = $("#division").val();
        var district = $("#district").val();
        var police = $("#police").val();
        var rating = $("#rating").val();
        var status = $("#status").val();
        $.ajax({
            url: "find",
            data: {"name": name, "nid": nid, "division": division, "district": district, "police": police, "rating": rating, "status": status},
            beforeSend: function () {
                $("#loader").show();
            },
            success: function (data) {
               var obj = jQuery.parseJSON(data);
                if(obj.success == "success"){
                $("#des").empty();
                $("#des").append(obj.detail);
                $('#des').fadeIn('fast');
                $("#loader").hide();
                
            }
            }


        })
    }

</script>

<?php
$empurl = Yii::app()->createUrl('admin/criminal/getdistrict');
Yii::app()->getClientScript()->registerScript('empscript', '
	$("#division").change(function(){
		var category_id = $("#division").val();
		$.ajax({
			url: "' . $empurl . '",
			data:{"category_id":category_id},
			beforeSend: function (){
                        $("#loader").show();    
                        },
			success: function (data) {
			var obj = jQuery.parseJSON(data);
			if(obj.success == "success"){
			$("#district").html(obj.dropList);
                        $("#loader").hide(); 
			}
			  
			   
		}
	  });
	return false; 
	});  
  ', CClientScript::POS_READY);


$empurls = Yii::app()->createUrl('admin/criminal/getstation');
Yii::app()->getClientScript()->registerScript('empssscript', '
	$("#district").change(function(){
		var category_id = $("#division").val();
                var ds_id = $("#district").val();
		$.ajax({
			url: "' . $empurls . '",
			data:{"category_id":category_id,"ds_id":ds_id},
			beforeSend: function (){
                        $("#loader").show();    
                        },
			success: function (data) {
			var obj = jQuery.parseJSON(data);
			if(obj.success == "success"){
			$("#police").html(obj.dropList);
                        $("#loader").hide();
			}
			  
			   
		}
	  });
	return false; 
	});  
  ', CClientScript::POS_READY);
?>
    