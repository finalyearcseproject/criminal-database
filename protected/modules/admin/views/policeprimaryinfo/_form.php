<?php
/* @var $this PoliceprimaryinfoController */
/* @var $model Policeprimaryinfo */
/* @var $form CActiveForm */
?>

<?php if (Yii::app()->user->hasFlash('success')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-success"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>
<?php if (Yii::app()->user->hasFlash('danger')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-danger"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>

 <h4> <i class="fa fa-plus-circle"></i> Add Police </h4><hr>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'policeprimaryinfo-form',
	'enableAjaxValidation'=>FALSE,
    'enableClientValidation'=>true,
     'htmlOptions'=>array('enctype' => 'multipart/form-data'),
    'clientOptions'=>array('validateOnSubmit'=>true, 'validateOnChange'=>true),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<div class="row">
    
    <div class="col-md-4">
	 <div class="form-group">
            <?php echo $form->labelEx($model, 'division_id'); ?>
            <?php echo $form->dropDownList($model, 'division_id', $DivisionList, array('empty' => '(Select a Division)', 'class' => 'form-control', 'id' => 'selectDV')); ?>
            <?php echo $form->error($model, 'division_id'); ?>
        </div>

	<div class="form-group">
            <?php echo $form->labelEx($model, 'district_id'); ?>
            <?php echo $form->dropDownList($model, 'district_id', array('empty' => 'Select District'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'district_id'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'station_id'); ?>
            <?php echo $form->dropDownList($model, 'station_id', array('empty' => 'Select Station'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'station_id'); ?>
        </div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name', array('class' => 'form-control'));?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'father_name'); ?>
		<?php echo $form->textField($model,'father_name', array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'father_name'); ?>
	</div>
    </div>
    
    <div class="col-md-4">

	<div class="form-group">
		<?php echo $form->labelEx($model,'mother_name'); ?>
		<?php echo $form->textField($model,'mother_name', array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'mother_name'); ?>
	</div>

	<div class="form-group">
            <?php echo $form->labelEx($model, 'sex'); ?>
            <?php echo $form->dropDownList($model, 'sex', array('MALE' => 'Male', 'FEMALE' => 'Female'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'sex'); ?>
        </div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nationality'); ?>
		<?php echo $form->textField($model,'nationality', array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'nationality'); ?>
	</div>

	<div class="form-group">
            <?php echo $form->labelEx($model, 'religion'); ?>
            <?php echo $form->dropDownList($model, 'religion', array('Hindu' => 'Hindu', 'Muslim' => 'Muslim', 'Buddho' => 'Buddho', 'Christian' => 'Christian'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'religion'); ?>
        </div>

	<div class="form-group">
            <?php echo $form->labelEx($model, 'blood_group'); ?>
            <?php echo $form->dropDownList($model, 'blood_group', array('A+' => 'A+', 'B+' => 'B+', 'O+' => 'O+', 'O-' => 'O-', 'AB+' => 'AB+', 'AB-' => 'AB-', 'A-' => 'A-', 'B-' => 'B-'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'blood_group'); ?>
        </div>
        
    </div>
    
    <div class="col-md-4">

	 <div class="form-group">
            <?php echo $form->labelEx($model, 'maretial_status'); ?>
            <?php echo $form->dropDownList($model, 'maretial_status', array('Married' => 'Married', 'Unmarried' => 'Unmarried', 'Divorsed' => 'Divorsed'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'maretial_status'); ?>
        </div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'national_id'); ?>
		<?php echo $form->textField($model,'national_id', array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'national_id'); ?>
	</div>

	<div class="form-group">
            <?php echo $form->labelEx($model, 'Upload Image'); ?>
            <?php
                      echo CHtml::activeFileField($model, 'image'); 
            ?>
        </div>

	<div class="form-group">
            <?php echo $form->labelEx($model, 'status'); ?>
            <?php echo $form->dropDownList($model, 'status', array('On Duty' => 'On Duty', 'Suspanded' => 'Suspanded', 'Retired' => 'Retired', 'Died' => 'Died','On Review'=>'On Review'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>

	<div class="form-group">
		<?php echo CHtml::submitButton('submit',array('class'=>'btn btn-success')); ?>
	</div>
        
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
$empurl = Yii::app()->createUrl('admin/criminal/getdistrict');
Yii::app()->getClientScript()->registerScript('empscript', '
	$("#selectDV").change(function(){
		var category_id = $("#selectDV").val();
		$.ajax({
			url: "' . $empurl . '",
			data:{"category_id":category_id},
			beforeSend: function (){
                        $("#loader").show();    
                        },
			success: function (data) {
			var obj = jQuery.parseJSON(data);
			if(obj.success == "success"){
			$("#Policeprimaryinfo_district_id").html(obj.dropList);
			}
			  
			   
		}
	  });
	return false; 
	});  
  ', CClientScript::POS_READY);


$empurls = Yii::app()->createUrl('admin/criminal/getstation');
Yii::app()->getClientScript()->registerScript('empssscript', '
	$("#Policeprimaryinfo_district_id").change(function(){
		var category_id = $("#selectDV").val();
                var ds_id = $("#Policeprimaryinfo_district_id").val();
		$.ajax({
			url: "' . $empurls . '",
			data:{"category_id":category_id,"ds_id":ds_id},
			beforeSend: function (){
                        $("#loader").show();    
                        },
			success: function (data) {
			var obj = jQuery.parseJSON(data);
			if(obj.success == "success"){
			$("#Policeprimaryinfo_station_id").html(obj.dropList);
			}
			  
			   
		}
	  });
	return false; 
	});  
  ', CClientScript::POS_READY);
?>