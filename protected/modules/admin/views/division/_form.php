<?php
/* @var $this DivisionController */
/* @var $model Division */
/* @var $form CActiveForm */
?>
 <?php if (Yii::app()->user->hasFlash('success')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-success"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>
<?php if (Yii::app()->user->hasFlash('danger')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-danger"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>
<div class="row">
<div class="col-md-6">
    
    <h4><i class="fa fa-plus-circle"></i> Add Division</h4><hr>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'division',
	'enableAjaxValidation'=>FALSE,
        'enableClientValidation'=>TRUE,
        'clientOptions'=>array('validateOnSubmit'=>true, 'validateOnChange'=>true),
)); ?>

	<?php //echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('class'=>'form-control','style'=>'width:200px')); ?>
           
		<?php echo $form->error($model,'name'); ?>
	</div><br>

	<div class="form-group">
		<?php echo CHtml::submitButton('Submit',array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>
</div>
<div class="col-md-6">
 <h4><i class="fa fa-th-list"></i> Division List</h4><hr>
<?php $data = Division::model()->findAll(); ?>
 <table id="example1" class="table table-bordered ">
     <thead>
         <tr><td>SN</td><td>Division</td><td>Action</td></tr></thead>
     <tbody>
     <?php $i=1;foreach($data as $value){ ?>
     <tr><td><?php echo $i; ?></td><td><?php echo $value['name']; ?></td><td><a href="<?php echo Yii::app()->createUrl('admin/division/delete',array('id'=>$value['id'])); ?>"  onclick="return confirm('Are you sure want to delete');"><i style="font-size: 15px;" class="fa fa-trash-o"></i></a></td></tr>
     <?php $i++;} ?>
     </tbody>
 </table>
</div>

</div>

<script type="text/javascript">
                $(function() {
                    $("#example1").dataTable({ 
                        "iDisplayLength": 5,

            });
                    $('#example2').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": true
                    });
                });
        </script>