<?php
/* @var $this StationController */
/* @var $model Station */
/* @var $form CActiveForm */
?>

 <?php if (Yii::app()->user->hasFlash('success')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-success"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>
<?php if (Yii::app()->user->hasFlash('danger')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-danger"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>



<div class="row">
    
     <div class="col-md-6">
        <h4> <i class="fa fa-plus-circle"></i> Add Police Station </h4> <hr>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'division-form',
	'enableAjaxValidation'=>FALSE,
    'enableClientValidation'=>true,
    'clientOptions'=>array('validateOnSubmit'=>true, 'validateOnChange'=>true),
)); ?>
	

	

	<div class="form-group">
		<?php echo $form->labelEx($model,'division_id'); ?>
		<?php echo $form->dropDownList($model,'division_id',$drop,array('class'=>'form-control','style'=>'width: 122px')); ?>
	<?php echo $form->error($model,'division_id'); ?>
        </div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'district_id'); ?>
		<?php echo $form->dropDownList($model,'district_id',array('empty'=>'Select District'),array('class'=>'form-control','style'=>'width: 122px')); ?>
	<?php echo $form->error($model,'district_id'); ?>
        </div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('class'=>'form-control','style'=>'width:150px')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton('submit',array('class'=>'btn btn-success')); ?>
	</div>
        
        <?php $this->endWidget(); ?>
     </div>
    
    
    <div class="col-md-6">
        
        <h4><i class="fa fa-th-list"></i> Station List</h4><hr>
        
        <?php $data= Station::model()->findAll();   ?>
        
        <table id="example1" class="table table-bordered">
            <thead>
            <tr><td>SN</td><td>Station</td><td>Action</td></tr>
            </thead>
            <tbody>
            <?php $i=1;foreach($data as $value){ ?>
            <tr><td><?php echo $i; ?></td> <td><?php echo $value['name']; ?></td><td><a href="<?php echo Yii::app()->createUrl('station/delete',array('id'=>$value['id'])); ?>"  onclick="return confirm('Are you sure want to delete');"><i style="font-size: 15px;" class="fa fa-trash-o"></i></a></td></tr>
            <?php $i++;} ?>
            </tbody>
        </table>
        
    </div>



</div>

<script type="text/javascript">
                $(function() {
                    $("#example1").dataTable({ 
                        "iDisplayLength": 5,

            });
                    $('#example2').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": true
                    });
                });
        </script>
        
        
   <?php     
        $empurls = Yii::app()->createUrl('admin/station/getdistrict');
Yii::app()->getClientScript()->registerScript('empssscript', '
	$("#Station_division_id").change(function(){
		var category_id = $("#Station_division_id").val();
                
		$.ajax({
			url: "' . $empurls . '",
			data:{"category_id":category_id},
			beforeSend: function (){
                        $("#loader").show();    
                        },
			success: function (data) {
			var obj = jQuery.parseJSON(data);
			if(obj.success == "success"){
			$("#Station_district_id").html(obj.dropList);
			}
			  
			   
		}
	  });
	return false; 
	});  
  ', CClientScript::POS_READY);
?>