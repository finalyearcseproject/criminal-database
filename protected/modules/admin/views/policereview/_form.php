<?php
/* @var $this PolicereviewController */
/* @var $model Policereview */
/* @var $form CActiveForm */
?>

<?php
if (Yii::app()->user->hasFlash('success')) {
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div font-Size: 20px; padding-top:20px", class="alert alert-success"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>' . $message . "</h4></b></div>\n";
    }
}
?>
<?php
if (Yii::app()->user->hasFlash('danger')) {
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div font-Size: 20px; padding-top:20px", class="alert alert-danger"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>' . $message . "</h4></b></div>\n";
    }
}
?>

<div class="row">

<?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'crimprsn-form',
        'enableAjaxValidation' => FALSE,
        'enableClientValidation' => true,
        'htmlOptions'=>array('enctype' => 'multipart/form-data'),
        'clientOptions' => array('validateOnSubmit' => true, 'validateOnChange' => true),
    ));
    ?>
    
    <div class="col-md-4" style="border-right: 1px solid #060">

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'header'); ?>
		<?php echo $form->textField($model,'header',array('class' => 'form-control','style'=>'width: 300px')); ?>
		<?php echo $form->error($model,'header'); ?>
	</div>

	<div class="form-group">
            
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date',array('class'=>'form-control','id'=>'dp1','style'=>'width: 300px','dateFormat:yy-mm-dd')); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description', array('class' => 'form-control','style'=>'width: 300px')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'police_id'); ?>
		<?php echo $form->textField($model,'police_id', array('class' => 'form-control','style'=>'width: 300px')); ?>
		<?php echo $form->error($model,'police_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id', array('class' => 'form-control','style'=>'width: 300px')); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="form-group">
		 <?php echo CHtml::submitButton('submit', array('class' => 'btn btn-success lg')); ?>
	</div>
        
    </div>

<?php $this->endWidget(); ?>
    <div class="col-md-8">
        <?php $data= Policereview::model()->findAll();   ?>
        
        <table id="example1" class="table table-bordered">
            <thead>
            <tr><td>SN</td><td>Header</td><td>Date</td><td>Description</td><td>Police ID</td><td>User Id</td><td>Update</td></tr>
            </thead>
            <tbody>
            <?php $i=1;foreach($data as $value){ ?>
            <tr><td><?php echo $i; ?></td> <td><?php echo $value['header']; ?><td><?php echo $value['date']; ?><td><?php echo $value['description']; ?></td><td><?php echo $value['police_id']; ?><td><td><?php echo $value['user_id']; ?><a href="<?php echo Yii::app()->createUrl('admin/district/delete',array('id'=>$value['id'])); ?>"  onclick="return confirm('Are you sure want to delete');"><i style="font-size: 15px;" class="fa fa-trash-o"></i></a></td></tr>
            <?php $i++;} ?>
            </tbody>
        </table>
    </div>

</div><!-- form -->