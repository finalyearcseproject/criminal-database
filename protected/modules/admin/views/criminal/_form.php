<?php
/* @var $this DistrictController */
/* @var $model District */
/* @var $form CActiveForm */
?>

<?php
if (Yii::app()->user->hasFlash('success')) {
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div font-Size: 20px; padding-top:20px", class="alert alert-success"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>' . $message . "</h4></b></div>\n";
    }
}
?>
<?php
if (Yii::app()->user->hasFlash('danger')) {
    foreach (Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div font-Size: 20px; padding-top:20px", class="alert alert-danger"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>' . $message . "</h4></b></div>\n";
    }
}
?>

<div class="row">

    <h4> <i class="fa fa-plus-circle"></i> Add Criminal </h4><hr>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'crimprsn-form',
        'enableAjaxValidation' => FALSE,
        'enableClientValidation' => true,
        'htmlOptions'=>array('enctype' => 'multipart/form-data'),
        'clientOptions' => array('validateOnSubmit' => true, 'validateOnChange' => true),
    ));
    ?>
    <div class="col-md-3" style="border-right: 1px solid #060">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'father_name'); ?>
            <?php echo $form->textField($model, 'father_name', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'father_name'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'mother_name'); ?>
            <?php echo $form->textField($model, 'mother_name', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'mother_name'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'siblings'); ?>
            <?php echo $form->textField($model, 'siblings', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'siblings'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'nationality'); ?>
            <?php echo $form->textField($model, 'nationality', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'nationality'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'national_id'); ?>
            <?php echo $form->textField($model, 'national_id', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'national_id'); ?>
        </div>

    </div>

    <div class="col-md-3" style="border-right: 1px solid #060">


        <div class="form-group">
            <?php echo $form->labelEx($model, 'religion'); ?>
            <?php echo $form->dropDownList($model, 'religion', array('Hindu' => 'Hindu', 'Muslim' => 'Muslim', 'Buddho' => 'Buddho', 'Christian' => 'Christian'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'religion'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'blood_group'); ?>
            <?php echo $form->dropDownList($model, 'blood_group', array('A+' => 'A+', 'B+' => 'B+', 'O+' => 'O+', 'O-' => 'O-', 'AB+' => 'AB+', 'AB-' => 'AB-', 'A-' => 'A-', 'B-' => 'B-'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'blood_group'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'sex'); ?>
            <?php echo $form->dropDownList($model, 'sex', array('MALE' => 'Male', 'FEMALE' => 'Female'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'sex'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'maretial_status'); ?>
            <?php echo $form->dropDownList($model, 'maretial_status', array('Married' => 'Married', 'Unmarried' => 'Unmarried', 'Divorsed' => 'Divorsed'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'maretial_status'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'status'); ?>

            <?php echo $form->dropDownList($model, 'status', $CriminalStatusDrop, array('empty' => '(Select a Status)', 'class' => 'form-control', 'style' => 'width: 208px')); ?>
            <?php echo $form->error($model, 'status'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'division_id'); ?>
            <?php echo $form->dropDownList($model, 'division_id', $DivisionList, array('empty' => '(Select a Division)', 'class' => 'form-control', 'id' => 'selectDV')); ?>
            <?php echo $form->error($model, 'division_id'); ?>
        </div>


    </div> 
    <div class="col-md-3" style="border-right: 1px solid #060">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'district_id'); ?>
            <?php echo $form->dropDownList($model, 'district_id', array('empty' => 'Select District'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'district_id'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'police_station'); ?>
            <?php echo $form->dropDownList($model, 'police_station', array('empty' => 'Select Station'), array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'police_station'); ?>
        </div>
        <div class="form-group">
            <?php //echo $form->labelEx($model, 'rating'); ?>
            <strong>Based on a 5 STAR SCALE, please rate The Criminal,
                5 stars representing Very Dangerous and 1 star representing Not Very Dangerous.</strong><br/><br/>
            <div class="row">

                <div class="col-sm-12">
                    <?php
                    $this->widget('CStarRating', array(
                        'model' => $model,
                        'attribute' => 'rating',
                        'name' => 'rating1',
                        'value' => 5,
                        'minRating' => 1,
                        'maxRating' => 5, //max value
                        'starCount' => 5, //number of stars
                    ));
                    ?>
                </div>
            </div>

<?php echo $form->error($model, 'rating'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'characteristics'); ?>
            <?php echo $form->textArea($model, 'characteristics', array('class' => 'form-control')); ?>
<?php echo $form->error($model, 'characteristics'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'organizetion'); ?>
            <?php echo $form->textField($model, 'organizetion', array('class' => 'form-control')); ?>
<?php echo $form->error($model, 'organizetion'); ?>
        </div>
    </div>
    <div class="col-md-3" style="border-right: 1px solid #060">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'comments'); ?>
            <?php echo $form->textArea($model, 'comments', array('class' => 'form-control')); ?>
<?php echo $form->error($model, 'comments'); ?>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'Upload Image'); ?>
            <?php
                     $this->widget('CMultiFileUpload', array(
                        'model'=>$model,
                        'attribute'=>'file',
                        'accept'=>'jpg|gif|png',
                        'options'=>array(
                           // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
                           // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
                           // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
                           // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
                           // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
                           // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
                        ),
                        'denied'=>'File is not allowed',
                        'max'=>10, // max 10 files
 
 
  ));
            ?>
        </div>

    </div>

</div><hr>
<div class="form-group">
    <?php echo CHtml::submitButton('submit', array('class' => 'btn btn-success lg', 'style' => 'float:right')); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(function () {
        $("#example1").dataTable({
            "iDisplayLength": 5,
        });
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true
        });
    });
</script>

<?php
$empurl = Yii::app()->createUrl('admin/criminal/getdistrict');
Yii::app()->getClientScript()->registerScript('empscript', '
	$("#selectDV").change(function(){
		var category_id = $("#selectDV").val();
		$.ajax({
			url: "' . $empurl . '",
			data:{"category_id":category_id},
			beforeSend: function (){
                        $("#loader").show();    
                        },
			success: function (data) {
			var obj = jQuery.parseJSON(data);
			if(obj.success == "success"){
			$("#Crimpersonalinfo_district_id").html(obj.dropList);
			}
			  
			   
		}
	  });
	return false; 
	});  
  ', CClientScript::POS_READY);


$empurls = Yii::app()->createUrl('admin/criminal/getstation');
Yii::app()->getClientScript()->registerScript('empssscript', '
	$("#Crimpersonalinfo_district_id").change(function(){
		var category_id = $("#selectDV").val();
                var ds_id = $("#Crimpersonalinfo_district_id").val();
		$.ajax({
			url: "' . $empurls . '",
			data:{"category_id":category_id,"ds_id":ds_id},
			beforeSend: function (){
                        $("#loader").show();    
                        },
			success: function (data) {
			var obj = jQuery.parseJSON(data);
			if(obj.success == "success"){
			$("#Crimpersonalinfo_police_station").html(obj.dropList);
			}
			  
			   
		}
	  });
	return false; 
	});  
  ', CClientScript::POS_READY);
?>