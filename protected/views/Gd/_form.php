<?php
/* @var $this GdController */
/* @var $model Gd */
/* @var $form CActiveForm */
?>
 <?php if (Yii::app()->user->hasFlash('success')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-success"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>
<?php if (Yii::app()->user->hasFlash('danger')) {
				foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div font-Size: 20px; padding-top:20px", class="alert alert-danger"' . $key . '"><button type="button" class="close" data-dismiss="alert">&times;</button><b><h4>'  . $message . "</h4></b></div>\n";
				}
				}
				?>



<div class="row">
    <div class="col-md-2">
    </div>
    
    <div class="col-md-8">
         <h4> <i class="fa fa-plus-circle"></i> General Diary</h4> <hr>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'division-form',
	'enableAjaxValidation'=>FALSE,
    'enableClientValidation'=>true,
    'clientOptions'=>array('validateOnSubmit'=>true, 'validateOnChange'=>true),
)); ?>

	

	<?php echo $form->errorSummary($model); ?>
         
         

	<div class="form-group">
		<?php echo $form->labelEx($model,'to'); ?>
		<?php echo $form->textField($model,'to',array('class'=>'form-control','size'=>60,'maxlength'=>255,'value'=>'Officer Incharge')); ?>
		<?php echo $form->error($model,'to'); ?>
	</div>
         
         

	
	<div class="form-group">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dacuments'); ?>
		<?php echo $form->textArea($model,'dacuments',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'dacuments'); ?>
	</div>
       

	<div class="form-group">
		
                <?php echo CHtml::submitButton('submit',array('class'=>'btn btn-success')); ?>
	</div>
</div>
    <div class="col-md-2">
        
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->