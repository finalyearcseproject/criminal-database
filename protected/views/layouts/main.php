<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />

    <!-- bootstrap CSS framework -->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/dataTables.bootstrap.css" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>



    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div style="width: 1326px" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('dashboard/index'); ?>">D</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Location's <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo Yii::app()->createUrl('division'); ?>"><b>Division</b></a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('district'); ?>">District</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('station'); ?>">Police Station</a></li>
                        </ul>
                    </li>
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Criminal's <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo Yii::app()->createUrl('criminal'); ?>"><b>Add Criminal</b></a></li>
                            
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">News <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo Yii::app()->createUrl('criminal'); ?>"><b>Add News</b></a></li>
                            
                        </ul>
                    </li>
                   
                
                   
                </ul>
                <ul class="navbar-nav navbar-right nav">

                    <li><a href="<?php echo Yii::app()->createUrl('site/logout'); ?>">Logout (<?php echo Yii::app()->user->getState('username'); ?>) </a></li>


                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

    <div class=""  style="margin-top: 70px;margin-left: auto;margin-right: auto;min-height: 400px;width: 1284px;">



<?php echo $content; ?>




    </div><br/>


    <footer class="footer navbar navbar-inverse " style="margin-top: 33px;">
        <div class="container">
            <a href="#" target="_blank"><p style="color:white;margin-top: 10px;" class="text-center">&copy;  <?php echo date('Y'); ?> </p></a>
        </div>
    </footer>

<!--<script rel="script" type="text/javascript" src="<?php //echo Yii::app()->request->baseUrl;  ?>/js/jquery-1.11.3.min.js"></script> -->     
    <script rel="script" type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>   
    <script rel="script" type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.dataTables.js"></script>
    <script rel="script" type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/dataTables.bootstrap.js"></script>       
    <script rel="script" type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
        $('#dp1').datepicker({
            format: 'mm/dd/yyyy',
        }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

        $('#dp2').datepicker({
            format: 'mm/dd/yyyy',
        }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
    });
    </script> 
</body>
</html>